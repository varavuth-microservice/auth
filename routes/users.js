const express = require('express');
const router = express.Router();
const argon2 = require('argon2');
const { QueryTypes } = require('sequelize');
const jwt = require('jsonwebtoken');
//iiy
const model = require('../models/index');
const passportJWT = require('../middlewares/passport-jwt')
/* /api/v1/users/ */

router.get('/profile', [passportJWT.isLogin],async function(req, res, next) {  
  const user = await model.User.findByPk(req.user.user_id);
  return res.status(200).json({
    user:{
      id:user.id,
      full:user.fullname,
      email:user.email,
      created_at:user.created_at
    }
  })
})
router.get('/', async function(req, res, next) {  
  const user = await model.User.findAll({
    //attributes:['id','fullname']  
    attributes:{exclude:['password']},
    order:[['id','desc']]
  });
  const sql = "SELECT id,fullname,email FROM `users` order by id desc";
  const users = await model.sequelize.query(sql, {
     type: QueryTypes.SELECT
     });

  const totalUsers = await model.User.count();

  return res.status(200).json({
    total:totalUsers,
    data: user
  });
});

/* /api/v1/users/register */
router.post('/register', async function(req, res, next) {
  const {fullname, email, password} = req.body;

  //1. เช็คอีเมล์ซ้ำ
  const user = await  model.User.findOne({ where: { email: email } });
  if (user !== null) {
    return res.status(409).json({message: 'มีผู้ใช้งาน email นี้แล้ว'});
  }
  
  //2. เข้ารหัสรหัสผ่าน
  const passwordHash = await argon2.hash(password);

  //3. บันทึกลงตาราง users
  const newUser = await model.User.create({
    fullname: fullname,
    email: email,
    password: passwordHash
  });

  return res.status(201).json({
    user: {
      id: newUser.id,
      fullname: newUser.fullname
    },
    message: 'ลงทะเบียนสำเร็จ'
  });
});
//login
router.post('/login', async function(req, res, next) {
  const {email, password} = req.body;

  //1. เช็คอีเมล์มีในระบบหรือไม่
  const user = await  model.User.findOne({ where: { email: email } });
  if (user === null) {
    return res.status(404).json({message: 'ไม่พบผู้ใช้งานนี้ในระบบ'});
  }
  
  //2. เช็ครหัสผ่าน
  const isValid = await argon2.verify(user.password,password);
  if (!isValid){
    return res.status(401).json({message: 'รหัสผ่านไม่ถูกต้อง'});
  }
  //3. สร้าง token
  const token = jwt.sign({user_id:user.id},process.env.JWT_KEY,{expiresIn:'7d'});
  

  return res.status(200).json({
    
    message: 'Login สำเร็จ',
    access_token: token
  });
});
module.exports = router;
